﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParticipantsViewerManagerDefault : ParticipantsViewerManager
{

    
    public Image m_imgProfile;
    public Text m_firstName;
    public Text m_lastName;
    public Text m_country;
    public Text m_hackathonRole;
    public Text m_language;

    public Text m_tIndex;
    public Text m_tParticipantsInSearch;

    public Transform[] m_switchOnNoResult;



    public Transform m_pPhoneNumber;
    public RedirectionButton m_pPhoneNumberRedirect;
    public Transform m_pMailId;
    public RedirectionButtonMail m_pMailIdRedirect;
    public Transform m_pLinkedIn;
    public RedirectionButtonWebsite m_pLinkedInRedirect;
    public Transform m_pTwitter;
    public RedirectionButtonWebsite m_pTwitterRedirect;
    public Transform m_pFacebook;
    public RedirectionButtonWebsite m_pFacebookRedirect;
    public Transform m_pWebsite;
    public RedirectionButtonWebsite m_pWebsiteRedirect;
    public Transform m_pSkype;
    public RedirectionButton m_pSkypeRedirect;

    protected override void RefreshInterfaceWith(Participant participant)
    {

        foreach (var toswitch in m_switchOnNoResult)
        {

            toswitch.gameObject.SetActive(participant != null);
        }
        if (participant == null)
        {
            m_firstName.text = "";
            m_lastName.text = "";
            m_imgProfile.sprite = null;
            m_country.text = "";
            m_hackathonRole.text = "";
            m_language.text = "";

        }
        else
        {

            m_firstName.text = LowerCase(participant.m_firstName);
            m_lastName.text = LowerCase(participant.m_lastName);
            m_imgProfile.sprite = participant.m_profilePicture;
            m_country.text = participant.m_originCountry;
            m_hackathonRole.text = GetParticipantRole(participant);
            m_language.text = participant.m_language;

        }

        if (GetCountParticipantsInSearch() > 0)
        {
            m_tIndex.text = "" + (GetCurrentIndex() + 1);
            m_tParticipantsInSearch.text = "" + GetCountParticipantsInSearch();
        }
        else
        {
            m_tIndex.text = "0";
            m_tParticipantsInSearch.text = "0";
        }
        SetRedirectionTo(m_pTwitter, m_pTwitterRedirect, participant.m_contacts.m_urlTwitter);
        SetRedirectionTo(m_pLinkedIn, m_pLinkedInRedirect, participant.m_contacts.m_urlLinkedIn);
        SetRedirectionTo(m_pFacebook, m_pFacebookRedirect, participant.m_contacts.m_urlFacebook);
        SetRedirectionTo(m_pWebsite, m_pWebsiteRedirect, participant.m_contacts.m_urlWebsite);
        SetRedirectionTo(m_pMailId, m_pMailIdRedirect, participant.m_contacts.m_mail);

    }

    private void SetRedirectionTo(Transform root, RedirectionButtonWebsite redirection, string url)
    {
        if (root == null || redirection == null)
            return;
        root.gameObject.SetActive(!string.IsNullOrEmpty(url));
        redirection.SetUrl(url);
    }
    private void SetRedirectionTo(Transform root, RedirectionButtonMail redirection, string mail, string subject ="First contact", string body = "Hello,\n\nPlease to meet you.\n\n Kind regards,\n[Name]"  )
    {
        if (root == null || redirection == null)
            return;
        root.gameObject.SetActive(!string.IsNullOrEmpty(mail));
        redirection.SetTo(mail, subject, body);
    }

    private string LowerCase(string value)
    {
        if (value == null || value.Length <= 2) return value;
        return value[0].ToString().ToUpper() + value.Substring(1, value.Length - 1).ToLower();
    }

    private static string GetParticipantRole(Participant participant)
    {
        string role = "";
        if (participant.m_role == Participant.Role.Unkown)
            role = "";
        else if (participant.m_role == Participant.Role.Other)
            role = participant.m_roleOther;
        else
            role = participant.m_role.ToString();
        
        return role;
    }
}