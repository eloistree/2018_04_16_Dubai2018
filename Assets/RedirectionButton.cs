﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RedirectionButton : MonoBehaviour {


    public abstract void Go();
}
