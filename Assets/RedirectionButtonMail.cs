﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectionButtonMail : RedirectionButton {

    public string m_mail;
    public string m_subject;
    public string m_body;

    public RedirectionButtonMail SetTo(string mail, string subject, string body) {
        m_mail=mail;
        m_subject = subject;
        m_body = body ;
        return this;
    }

    public override void Go()
    {
        SendMailTo.SendMailCrossPlatform.SendEmail(m_mail, m_subject, m_body);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
