﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectionButtonWebsite : RedirectionButton {

    public string m_url;

    public RedirectionButtonWebsite SetUrl(string url) {
        m_url = url;
        return this;
    }
    public override void Go()
    {
        Application.OpenURL(m_url);
    }
    
}
