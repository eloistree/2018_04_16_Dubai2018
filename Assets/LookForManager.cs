﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookForManager : MonoBehaviour {

    public InputField m_inputManager;
    public ParticipantsViewerManager m_participantsManager;
	// Use this for initialization
	void Start () {

        m_inputManager.onValueChanged.AddListener(LookForInfo);	
	}

    private void LookForInfo(string info)
    {
        if (m_participantsManager)
            m_participantsManager.LookParticipantWithFollowingInfo(info);
    }

    public void Reset()
    {
        m_participantsManager=  FindObjectOfType<ParticipantsViewerManager>();
    }

}
