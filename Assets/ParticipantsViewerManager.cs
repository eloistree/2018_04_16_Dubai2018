﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public abstract class ParticipantsViewerManager : MonoBehaviour {

    [SerializeField]
    private Participants m_participants;
    protected Participant [] m_participantsInSearch;

    [Header("Debug")]

    public int m_index;

    public void Awake()
    {
        SetParticipants(m_participants);
        RefreshInterfaceWith(m_index);
    }


    internal void LookParticipantWithFollowingInfo(string info)
    {
        Participant[] participants = m_participants.GetParticipants();
        var participantSelected = from selection in participants
                                  where GetScoreof(info, selection) > 0
                                  orderby GetScoreof(info, selection)
                                  select selection;
        m_participantsInSearch = participantSelected.ToArray<Participant>();
        Participant result = null;
        if (m_participantsInSearch.Length>0)
            result = participantSelected.First<Participant>();

        m_index = GetIndexOfResult(result);
        RefreshInterfaceWith(m_index);


    }

    private int GetIndexOfResult(Participant selection)
    {
        for (int i = 0; i < m_participantsInSearch.Length; i++)
        {

            if (m_participantsInSearch[i] == selection)
                return i;

        }
        return 0;

    }

    internal int GetCountParticipantsInSearch()
    {
        return m_participantsInSearch.Length;
    }

    internal int GetCurrentIndex()
    {
        return m_index;
    }

    private int GetScoreof(string info, Participant selection)
    {
        int score = 0;
        score += GetScoreOf(info, selection.m_firstName);
        score += GetScoreOf(info, selection.m_lastName);
        score += GetScoreOf(info, selection.m_originCountry);
        score += GetScoreOf(info, selection.m_phoneNumber);
        return score;

    }

    private int GetScoreOf(string info, string valueToCheck)
    {
        if (string.IsNullOrEmpty(valueToCheck))
            return 0;
        info = info.ToLower();
        valueToCheck = valueToCheck.ToLower();

        if (valueToCheck.StartsWith(info))
            return 4;
        if (valueToCheck.Contains(info))
            return 2;
        return 0;
    }

    
    private void RefreshInterfaceWith(int index)
    {
        if (m_participantsInSearch.Length == 0)
            RefreshInterfaceWith(null);
        else RefreshInterfaceWith(m_participantsInSearch[index]);
    }
    protected abstract void RefreshInterfaceWith(Participant participant);

    public void Previous()
    {

        m_index--;
        if (m_index < 0)
            m_index = m_participantsInSearch.Length-1;

        RefreshInterfaceWith(m_index);
    }

    public void Next()
    {
        m_index++;
        if (m_index >= m_participantsInSearch.Length)
            m_index = 0;
        RefreshInterfaceWith(m_index);
    }
    public void SetParticipants(Participants participants) {
        m_participants = participants;
        m_participantsInSearch = participants.GetParticipants();
    }
  
}




[System.Serializable]
public class Participants
{

    public List<Participant> m_participants = new List<Participant>();
    public int GetCount() { return m_participants.Count; }
    public void AddParticipant(Participant participant)
    {
        m_participants.Add(participant);
    }

    internal Participant GetParticipant(int index)
    {
        return m_participants[index];
    }

    internal Participant[] GetParticipants()
    {
        return m_participants.ToArray();
    }
}

[System.Serializable]
public class Participant
{

    public string m_firstName;
    public string m_lastName;
    public string m_originCountry;
    public string m_language;
    public Sprite m_profilePicture;
    public Specialty specialty = Specialty.Unkown;
    public enum Specialty {Unkown, Graphics, Coding, Buisness }
    public Role m_role = Role.Unkown;
    public string m_roleOther;
    public enum Role { Unkown, Organiser, Mentor, Participant, Other }

    public Contacts m_contacts;
    internal string m_phoneNumber;
}
[System.Serializable]
public class Contacts
{

    public string m_mail;
    public string m_phoneNumber;

    public string m_urlWebsite;
    public string m_urlLinkedIn;
    public string m_urlTwitter;
    public string m_urlFacebook;

    public string m_pSkypeId;


}