﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMailTo : MonoBehaviour {

    public string email;
    public string subject;
    [TextArea(2, 5)]
    public string body;

    public void SendMailWithParams()
    {
        SendMailCrossPlatform.SendEmail(email, subject, body);
    }

    /// <summary>
    /// - opens the mail app on android, ios & windows (tested on windows)
    /// </summary>
    public class SendMailCrossPlatform
    {

        public static void SendEmail(string email, string subject, string body)
        {
            subject = MyEscapeURL(subject);
            body = MyEscapeURL(body);
            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        }
        public static string MyEscapeURL(string url)
        {
            return WWW.EscapeURL(url).Replace("+", "%20");
        }
    }
}
